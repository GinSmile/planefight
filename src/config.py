# -*- coding: UTF-8 -*-
#游戏中使用的图形
plane_image = '../res/plane.png'
enemy_image = '../res/enemy.png'
splash_image = '../res/enemy.png'
bullet_image = '../res/bullet.png'

#设置窗口大小背景色等
screen_size = 300, 600
background_color = 255, 255, 255
margin = 30
full_screen = 0
font_size = 22

drop_speed = 7
plane_speed = 10
bullet_speed = 13
speed_increase = 1
enemys_per_level = 20
plane_pad_top = 40
plane_pad_side = 20

shoot = False