# -*- coding: UTF-8 -*-
import pygame, config
from random import randrange


class Sprite(pygame.sprite.Sprite):
    def __init__(self, image):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(image).convert()
        self.rect = self.image.get_rect()
        screen = pygame.display.get_surface()
        shrink = -config.margin * 2
        self.area = screen.get_rect().inflate(shrink, shrink)

class Enemy(Sprite):

    def __init__(self, speed):
        Sprite.__init__(self, config.enemy_image)
        self.speed = speed
        self.reset()

    def reset(self):
        x = randrange(self.area.left, self.area.right)
        self.rect.midbottom = x, 0

    def update(self):
        self.rect.top += self.speed
        self.landed = self.rect.top >= self.area.bottom

class Plane(Sprite):

    def __init__(self):
        Sprite.__init__(self, config.plane_image)
        self.rect.bottom = self.area.bottom
        self.pad_top = config.plane_pad_top
        self.pad_side = config.plane_pad_side


    def update(self):
        self.rect.centerx = pygame.mouse.get_pos()[0]
        self.rect.centery = pygame.mouse.get_pos()[1]
        self.rect = self.rect.clamp(self.area)


    def touches(self, other):
        bounds = self.rect.inflate(-self.pad_side, -self.pad_top)
        bounds.bottom = self.rect.bottom
        return bounds.colliderect(other.rect)


class Bullet(Sprite):
    def __init__(self, plane):
        Sprite.__init__(self, config.bullet_image)
        self.rect.centerx = plane.rect.centerx
        self.rect.centery = plane.rect.centery

    def update(self):
        self.rect.centery -= config.bullet_speed
        if self.rect.centery < 0:
            self.landed = 1
        
    def touches(self, other):
        return self.rect.colliderect(other.rect)  

