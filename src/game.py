# -*- coding: UTF-8 -*-
import os, sys, pygame
import objects, config
from pygame.locals import *

class State:
    def handle(self,event):
        if event.type == QUIT:
            sys.exit()
        if event.type == KEYDOWN and event.key == K_ESCAPE:
            sys.exit()
        if event.type == MOUSEBUTTONDOWN:
            config.shoot = True

    def firstDisplay(self, screen):
        screen.fill(config.background_color)
        pygame.display.flip()

    def display(self, screen):
        pass

class Level(State):
    #每个level的敌机数量一定，用子弹干掉或者躲开都可以
    def __init__(self,number=1):
        pygame.mouse.set_visible(False)

        #用来计算敌机出现的时间
        self.time = pygame.time.get_ticks()
        self.interval = 2000 - 2 * number #最多50关

        self.number = number
        self.score = 0
        self.remaining = config.enemys_per_level + 3 * number

        self.speed = config.drop_speed

        self.speed += (self.number - 1) * config.speed_increase
        
        #初始化玩家的飞机，敌机，以及子弹
        self.plane = objects.Plane()
        self.enemy = [objects.Enemy(self.speed)]
        self.bullet = []
        self.planes = [self.plane] + self.enemy
        self.sprites = pygame.sprite.RenderUpdates(self.planes)
        

    def update(self, game):
        self.sprites.update()

        if config.shoot:
                config.shoot = False
                self.shoot()
        if pygame.time.get_ticks() - self.time > self.interval:
            self.time = pygame.time.get_ticks()
            enemy = objects.Enemy(self.speed)
            self.enemy.append(enemy)
            self.planes.append(enemy)
            self.sprites = pygame.sprite.RenderUpdates(self.planes)




        #看子弹是否打到敌人
        for enemy in self.enemy:
            for bullet in self.bullet:
                #看子弹有没有过界，过界的话从类中删除
                if bullet.rect.centery < 0:
                    self.bullet.remove(bullet)
                    self.planes.remove(bullet)
                    self.sprites = pygame.sprite.RenderUpdates(self.planes) 


                #检测子弹是否射中敌机
                if bullet.touches(enemy):       
                    self.score += 10
                    print self.score
                    #在self.sprites中移除此bullet和敌人
                    self.planes.remove(bullet)
                    self.planes.remove(enemy)
                    self.bullet.remove(bullet)
                    self.enemy.remove(enemy)

                    self.sprites = pygame.sprite.RenderUpdates(self.planes) 
        
        #看敌人是否碰到自己
        for enemy in self.enemy:
            if self.plane.touches(enemy):
                    game.nextState = GameOver()
        
        if self.remaining <= 0:
                    game.nextState = LevelCleared(self.number)        

    def display(self, screen):
        screen.fill(config.background_color)
        updates = self.sprites.draw(screen)
        pygame.display.update(updates)

    def shoot(self):
        bullet = objects.Bullet(self.plane)
        self.planes.append(bullet)
        self.bullet.append(bullet)
        self.sprites = pygame.sprite.RenderUpdates(self.planes)



class Paused(State):
    finished = 0
    image = None
    text = ''

    def handle(self, event):
        State.handle(self, event)
        if event.type in [MOUSEBUTTONDOWN,KEYDOWN]:
            self.finished = 1

    def update(self, game):
        if self.finished:
            game.nextState = self.nextState()

    def firstDisplay(self, screen):
        pygame.mouse.set_visible(True)
        screen.fill(config.background_color)

        font = pygame.font.Font("../res/font.ttf", config.font_size)
        lines = self.text.strip().splitlines()

        height = len(lines) * font.get_linesize()

        center,top = screen.get_rect().center
        top -= height // 2

        if self.image:
            image = pygame.image.load(self.image).convert()

            r = image.get_rect()
            top += r.height // 2

            r.midbottom = center, top -20

            screen.blit(image, r)

        antialias = 1
        black = 0,0,0


        for line in lines:
            text = font.render(line.strip(),antialias,black)
            r = text.get_rect()
            r.midtop = center,top
            screen.blit(text, r)
            top += font.get_linesize()

        pygame.display.flip()

class Info(Paused):

    nextState = Level
    text = u'''
    准备.'''

class StartUp(Paused):

    nextState = Info
    image = config.splash_image
    text = u'''
    欢迎来到飞机大战，点击开始'''

class LevelCleared(Paused):

    def __init__(self, number):
            self.number = number
            self.text = u'''Good! 等级 %i 搞定，
            点击开始下一关''' % self.number

    def nextState(self):
            return Level(self.number + 1)

class GameOver(Paused):
    nextState = Level
    text = u'''
    游戏结束！
    点击鼠标重新开始, 按Esc键退出'''


class Game:

    def __init__(self,*args):
        path = os.path.abspath(args[0])
        dir = os.path.split(path)[0]
        os.chdir(dir)
        self.state = None

        self.nextState = StartUp()

    def run(self):
        pygame.init()
        pygame.mouse.set_visible(True)

        flag = 0

        if config.full_screen:
                flag = FULLSCREEN
        screen_size = config.screen_size
        screen = pygame.display.set_mode(screen_size, flag)

        pygame.display.set_caption('飞机大战')


        # Clock对象
        clock = pygame.time.Clock()

        while True:
            clock.tick(60)
            if self.state != self.nextState:
                self.state = self.nextState
                self.state.firstDisplay(screen)

            for event in pygame.event.get():                
                self.state.handle(event)

            self.state.update(self)

            self.state.display(screen)
            pygame.display.flip()

if __name__ == '__main__':
    game = Game(*sys.argv)
    game.run()